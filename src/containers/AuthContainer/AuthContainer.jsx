import React, { Component, Fragment } from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import { connect } from "react-redux";

import Login from "../../components/Auth/Login/Login";
import NotFound from "../../components/NotFound/NotFound";

import { closeErrorSnack } from "../../actions/appActions";

import "./AuthContainer.scss";

class AuthContainer extends Component {
    render() {
        const { match } = this.props;
        if (!!localStorage.token) return <Redirect to="/main" />;
        return (
            <Fragment>
                <main className="auth_container">
                    <div className="auth-box">
                        <div className="auth_bg"></div>
                        <div className="auth_content">
                            <Switch>
                                <Route path={`${match.url}/login`} exact component={Login} />
                                <Route render={() => <NotFound />} />
                            </Switch>
                        </div>
                    </div>
                </main>
            </Fragment>
        );
    }
}

const mapStateToProps = ({ app }) => {
    return {
        errorSnack: app.errorSnack,
        errorSnackText: app.errorSnackText
    };
};

const mapDispatchToProps = {
    closeErrorSnack
};

export default connect(mapStateToProps, mapDispatchToProps)(AuthContainer);
