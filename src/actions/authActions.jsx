import { AUTH } from "../constants";

export function postLogin(data) {
    return {
        type: AUTH.LOGIN,
        payload: {
            client: "default",
            request: {
                url: `/auth/sign-in/`,
                method: "post",
                data
            }
        }
    };
}
