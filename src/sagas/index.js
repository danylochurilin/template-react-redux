import { put, takeEvery, all } from "redux-saga/effects";
import { handleProfileActions, handleProfileLoader, handleProfileSnacks } from "./profile";

export default function* rootSaga() {
    yield all([handleLoader(), handleSnack(), handleProfileActions(), handleProfileLoader(), handleProfileSnacks()]);
}

const delay = ms => new Promise(res => setTimeout(res, ms));

export function* openErrorSnack(e) {
    yield put({
        type: "ERROR_SNACK_OPEN",
        payload:
            e && e.error && e.error.response && e.error.response.data
                ? e.error.response.data.detail
                : e.error && e.error.data
                ? e.error.data
                : e.error
                ? e.error
                : "Something went wrong"
    });
}

export function* openSuccessSnack(e) {
    yield put({ type: "SUCCESS_SNACK_OPEN", payload: e });
}

export function* resetErrorSnack() {
    yield delay(100);
    yield put({ type: "ERROR_SNACK_CLOSE" });
}

export function* resetSuccessSnack() {
    yield delay(100);
    yield put({ type: "SUCCESS_SNACK_CLOSE" });
}

export function* handleSnack() {
    yield takeEvery("ERROR_SNACK_OPEN", resetErrorSnack);
    yield takeEvery("SUCCESS_SNACK_OPEN", resetSuccessSnack);
}

export function* enableLoader() {
    yield put({ type: "LOADING", payload: true });
}

export function* disableLoader() {
    yield put({ type: "LOADING", payload: false });
}

export function* enableButtonLoader() {
    yield put({ type: "BUTTON_LOADING", payload: true });
}

export function* disableButtonLoader() {
    yield put({ type: "BUTTON_LOADING", payload: false });
}

function* handleLoader() {
    yield takeEvery("LOGIN", enableLoader);
    yield takeEvery("LOGIN_SUCCESS", disableLoader);
    yield takeEvery("LOGIN_FAIL", disableLoader);

    yield takeEvery("LOGIN", enableButtonLoader);
    yield takeEvery("LOGIN_SUCCESS", disableButtonLoader);
    yield takeEvery("LOGIN_FAIL", disableButtonLoader);
}
