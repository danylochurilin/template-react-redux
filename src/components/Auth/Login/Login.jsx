import React, { Component } from "react";
import { Field, reduxForm } from "redux-form";
// import { Field, reduxForm, SubmissionError } from 'redux-form';
import { connect } from "react-redux";
import ErrorIcon from "@material-ui/icons/Error";
import { postLogin } from "../../../actions/authActions";
import RenderField from "../../HelperComponents/RenderField/RenderField";
import DefaultButton from "../../HelperComponents/Buttons/DefaultButton/DefaultButton";
import TooltipMessage from "../../HelperComponents/TooltipMessage/TooltipMessage";

class Login extends Component {
    componentDidMount() {
        const { history } = this.props;
        if (localStorage.token) {
            history.push("/main");
        }
    }

    submitForm = data => {
        const { postLogin, history } = this.props;
        return postLogin(data).then(res => {
            if (res.payload && res.payload.status && res.payload.status === 200) {
                localStorage.token = res.payload.data.token;
                history.push("/main");
            } else {
                // throw new SubmissionError({ ...res.error.response.data, _error: res.error.response.data.detail });
            }
        });
    };

    render() {
        const { handleSubmit, submitting, pristine, valid, authError, loading } = this.props;

        return (
            <form onSubmit={handleSubmit(this.submitForm)}>
                <div className="auth_block_head">Sign in to your account</div>
                <div className="auth_block_descriptions">Enter details below</div>
                <Field name="email" type="text" component={RenderField} label="Email" />
                <Field name="password" type="password" component={RenderField} label="Password" />
                <div className="auth_btn_wrapper">
                    <DefaultButton
                        variant="contained"
                        disabled={submitting || pristine || !valid}
                        loading={loading}
                        formAction
                    >
                        Sign in
                    </DefaultButton>
                    {authError ? (
                        <TooltipMessage text={authError} delay={200} error position="right" classes="">
                            <ErrorIcon />
                        </TooltipMessage>
                    ) : (
                        ""
                    )}
                </div>
            </form>
        );
    }
}

const validate = values => {
    const errors = {};
    if (!values.email) {
        errors.email = "Required";
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,6}$/i.test(values.email)) {
        errors.email = "Invalid email";
    }
    if (!values.password) {
        errors.password = "Required";
    } else if (values.password.length < 8) {
        errors.password = "Must be 8 characters or more";
    }
    return errors;
};

Login = reduxForm({
    form: "LoginForm",
    validate
})(Login);

const mapStateToProps = ({ auth, app }) => {
    return {
        authError: auth.error_auth,
        loading: app.loading
    };
};
const mapDispatchToProps = {
    postLogin
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
