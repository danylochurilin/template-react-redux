let BASE_URL, SECOND_URL;

const host = window.location.host;

if (host === "admin.campy.local") {
    BASE_URL = "http://192.168.1.151:33000";
} else if (host.includes("localhost")) {
    BASE_URL = "https://api.template.4-com.pro";
} else {
    BASE_URL = `https://api.template.4-com.pro`;
}

SECOND_URL = "";

export const API_BASE_URL = BASE_URL;
export const API_SECOND_URL = SECOND_URL;