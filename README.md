# Template README #

### Getting started ###

To get the frontend project running locally:
* Clone this repo
* npm install to install all required dependencies
* npm start to start the local server (this project uses create-react-app)
###
Local web server will use standard React's port 3000. You can configure port in scripts section of package.json: add "-p <port>" to "start" script.

### Libraries ###

* react: ^16.12.0
* react-redux: ^6.0.0
* redux: ^4.0.1
* @emotion/core: ^10.0.27
* @material-ui/core: ^4.7.0
* axios: ^0.18.0
* connected-react-router: ^6.3.2
* history: ^4.7.2
* moment: ^2.24.0
* node-sass: ^4.12.0
* query-string: 6.8.2
* react-addons-css-transition-group: ^15.6.2
* react-date-range: ^1.0.3
* react-dom: ^16.7.0
* react-paginate: ^6.3.2
* react-responsive: ^7.0.0
* react-router-dom: ^4.3.1
* react-scripts: 2.1.3
* react-select: ^3.0.8
* react-spinners: ^0.8.0
* redux-axios-middleware: ^4.0.0
* redux-form: ^8.1.0

### Functionality overview ###

The application is a react-redux template. It uses a custom API for all requests, including authentication.
###

##### General functionality: #####

* Authenticate user (log in/sign up/password recovery pages + logout button in app header)
* 

##### The general page breakdown looks like this: #####

* Sign in/Sign up pages (URL: /#/auth/login, /#/auth/sign-in, /#/auth/sign-up, /#/auth/forgot-password)
* 

### Deployment ###

To get the frontend project deployed:
* Clone this repo
* npm install to install all required dependencies
* npm build to create an optimized production build
###
Now you can deploy your build folder to whatever server you need. 